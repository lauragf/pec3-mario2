﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMario : MonoBehaviour {

    /*
    public Transform target;

    public Vector3 offset = new Vector3(0.01f, 0f, -10f);

    public float dampTime = 0.03f;

    public Vector3 velocity = Vector3.zero;

    private void Awake(){
        Application.targetFrameRate = 60;
    }

    private void Update()
    {
        Camera camera = GetComponent<Camera>();

        Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
        Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(offset.x, offset.y, point.z));
        Vector3 destination = point + delta;

        destination = new Vector3(destination.x, offset.y, offset.z);
        this.transform.position = Vector3.SmoothDamp(this.transform.position, destination, ref velocity, dampTime);
    }
    
    */
    // OLD CAMERA
    public Transform pj;
    public float suave = 5f;
    public float dampTime = 0.03f;
    public Vector3 velocity = Vector3.zero;
    public Vector3 ajusteCam = new Vector3( 0, 0, 0);
    Vector3 desfase;
    

        // Use this for initialization
        void Start () {
            desfase = transform.position - pj.position;
   
        }

        // Update is called once per frame
        void FixedUpdate () {
            Vector3 posicionPj = pj.position + desfase;

            //transform.position = Vector3.Lerp(transform.position, posicionPj, suave * Time.deltaTime);
            transform.position = Vector3.SmoothDamp(transform.position, posicionPj, ref velocity, suave * Time.deltaTime) ;
    }
       
}
