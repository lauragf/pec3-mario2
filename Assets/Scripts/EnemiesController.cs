﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour {

    public float maxSpeed = 0.2f;
    public float speed = 0.02f;
    public bool direction ;

    private Rigidbody2D rb2d;

    
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
	}


	
	
	void FixedUpdate () {

        if (GameManager.sharedInstance.currentGameState == GameState.inGame)
        {
            if (direction == true)
        {
            Debug.Log("Cambiamos direccion");
            this.transform.eulerAngles = new Vector3(0, 180.0f, 0);
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        }
        else
        {
            this.transform.eulerAngles = new Vector3(0, 0f, 0);
            rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);

        }

        
          
            if (this.rb2d.velocity.x < speed) {
                rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
            }
        }
    }

    void ChangeDirection()

    {
        direction = !direction;
    }

    //Colisiones Enemigo
    void OnTriggerEnter2D(Collider2D other)
    {

     
        if (other.gameObject.tag == "vacio")
        {
            Debug.Log("Adios Goomba");
            Destroy(gameObject);
            
        }

        if (other.gameObject.tag == "caida")
        {
            Debug.Log("Caida");
           

        }

        if (other.gameObject.tag == "Fire")
        {
            Debug.Log("MUERRE Goomba");
            Destroy(gameObject);

        }

        if (other.gameObject.tag == "Muro" || other.gameObject.tag != "Player")
        {
            Debug.Log("cambia direccion");
            ChangeDirection();

            


        }


    }

   
}
