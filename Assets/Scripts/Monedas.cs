﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monedas : MonoBehaviour {

    bool isCollected = false;

    public int value = 1;
   


      //Activar moneda
    public void Show() {
        this.GetComponent<SpriteRenderer>().enabled = true;
        this.GetComponent<CircleCollider2D>().enabled = true;
        isCollected = false;
    }

    //Desactivar moneda
    void Hide() {
        this.GetComponent<SpriteRenderer>().enabled = false;
        this.GetComponent<CircleCollider2D>().enabled = false;
    }

    void Collected() {
        isCollected = true;
        Hide();
        GameManager.sharedInstance.CollectObject(value);
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (otherCollider.tag == "Player") {
            Collected();
        }
    }


   
}
