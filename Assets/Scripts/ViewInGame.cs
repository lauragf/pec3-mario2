﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewInGame : MonoBehaviour {

    public Text coinsLabel;
    

	// Update is called once per frame
	void Update () {

        if (GameManager.sharedInstance.currentGameState == GameState.inGame) {
            int coins = GameManager.sharedInstance.collectedObjects;
            this.coinsLabel.text = coins.ToString();
        }
	}
}
