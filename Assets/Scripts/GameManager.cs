﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public enum GameState {
        menu,
        inGame,
        dead,
        gameOver,
        winner

}


public class GameManager : MonoBehaviour {

    public static GameManager sharedInstance;

    public GameState currentGameState = GameState.menu;

    public Canvas menuCanvas, gameOverCanvas, gameCanvas, winnerCanvas;

    public GameObject gameCanvasAudio, dieCanvasAudio, winCanvasAudio;

    public int collectedObjects = 0;

    public Monedas coin;

    //audio
    public AudioSource musicTheme;
    public AudioSource deadTheme;
    public AudioSource winTheme;





    private void Awake()
    {
        sharedInstance = this;
    }

    private void Start()
    {

        BackToMenu();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel") && Input.GetKeyDown(KeyCode.Escape)){
            QuitGame();
        }
    }

    public void StartGame () {

        
        SetGameState(GameState.inGame);

        musicTheme = gameCanvasAudio.GetComponent<AudioSource>();
        deadTheme = dieCanvasAudio.GetComponent<AudioSource>();
        winTheme = winCanvasAudio.GetComponent<AudioSource>();

       // musicTheme.Play();
       // deadTheme.Stop();
       // winTheme.Stop();

        ShowCollectObject();
       //RespawnEnemigos();
        PlayerController.sharedInstace.StartGame();

        this.collectedObjects = 0;    	
	}
	
	public void RestarGame()
    {
        SceneManager.LoadScene("Level_1");

    }
	public void GameOver () {
       // SetGameState(GameState.dead);
        SetGameState(GameState.gameOver);

    }

    public void Winner()
    {
        SetGameState(GameState.winner);

    }

    public void BackToMenu(){

        SetGameState(GameState.menu);

    }

    void SetGameState(GameState newGameState) {

        if (newGameState == GameState.menu){

            menuCanvas.enabled = true;
            gameOverCanvas.enabled = false;
            gameCanvas.enabled = false;
            winnerCanvas.enabled = false;

        }
        else if (newGameState == GameState.inGame){
        
            menuCanvas.enabled = false;
            gameOverCanvas.enabled = false;
            gameCanvas.enabled = true;
            winnerCanvas.enabled = false;

        }
        else if (newGameState == GameState.gameOver) {
         
            menuCanvas.enabled = false;
            gameOverCanvas.enabled = true;
            gameCanvas.enabled = false;
            winnerCanvas.enabled = false;

        }
        else if (newGameState == GameState.winner)
        {
          
            menuCanvas.enabled = false;
            gameOverCanvas.enabled = false;
            gameCanvas.enabled = false;
            winnerCanvas.enabled = true;

        }

        this.currentGameState = newGameState;

    }

    public void QuitGame() {
        Application.Quit();
    }

    public void CollectObject(int ObjectValue) {
        this.collectedObjects += ObjectValue;
    }

    public void ShowCollectObject()
    {
        GameObject[] Monedaslist = GameObject.FindGameObjectsWithTag("coin");
        if (Monedaslist.Length == 0)
        {
            Debug.Log("No hay monedas");
        }
    
        foreach (GameObject moneda in Monedaslist)
        {
            moneda.GetComponent<SpriteRenderer>().enabled = true;
            moneda.GetComponent<CircleCollider2D>().enabled = true;
        }
       
        
    }

    public void RespawnEnemigos()
    {
        GameObject[] enemigoslist = GameObject.FindGameObjectsWithTag("Enemigo");
        if (enemigoslist.Length == 0)
        {
            Debug.Log("No hay Enemigos");
        }

        foreach (GameObject enemigo in enemigoslist)
        {
            enemigo.GetComponent<SpriteRenderer>().enabled = true;
            enemigo.GetComponent<CircleCollider2D>().enabled = true;
        }


    }
}
