﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public static PlayerController sharedInstace;


    //variables velocidad y movimiento
    public float velX = 0.1f;
    public float maxSpeed = 2f;
    public float movX;
    public float inputX;
    public float posicionActual;

    //Variables Salto
    public float fuerzaSalto = 400f;
    public Transform pie;
    public float radioPie;
    public LayerMask suelo;
    public bool enSuelo;

    //variable BigMario
    public bool BigMario;

    //Player
    private Rigidbody2D rb2d;

    //Efectos audio
    private AudioSource audioPlayer;

    public AudioClip jump;
    public AudioClip fireBall;

    //Game Canvas
    public GameObject GameCanvas;

    //Posicion inicial juego
    private Vector3 posicionInicial;

    //Animacion
    Animator animator;

    //Jugador
    public GameObject player;

    //Polvo al caminar
    public ParticleSystem polvo;

    //Disparo Fuego
    public Transform bulletSpawner;
    public GameObject bulletPrefab;

    void Awake(){
        sharedInstace = this;
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        posicionInicial = this.transform.position;
    }

    public void StartGame () {
        Debug.Log("Moviendo");
        this.transform.position = posicionInicial;
        audioPlayer = GetComponent<AudioSource>();
    }
	
	void FixedUpdate () {


        if (GameManager.sharedInstance.currentGameState == GameState.inGame)
        {
            Fire();

            float inputX = Input.GetAxis("Horizontal"); //movimiento eje X

            float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

            //drch
            if (inputX > 0)
            {
                movX = transform.position.x + (inputX * velX);
                transform.position = new Vector3(movX, transform.position.y, 0);
                transform.localScale = new Vector3(1f, 1f, 1f);
                //animator.SetFloat("velX", inputX);
            }

            //izq
            if (inputX < 0)
            {
                movX = transform.position.x + (inputX * velX);

                transform.position = new Vector3(movX, transform.position.y, 0);
                transform.localScale = new Vector3(-1f, 1f, 1f);
                //animator.SetFloat("velX", Mathf.Abs(inputX));
            }

            //Para solucionar que mire para otro lado en la animacion cuando para
            if (inputX != 0)
            {
                animator.SetFloat("velX", 1);
            }
            else
            {
                animator.SetFloat("velX", 0);
            }



            //salto
            enSuelo = Physics2D.OverlapCircle(pie.position, radioPie, suelo);//salto solo desde suelo
            Debug.Log("Suelo = " + enSuelo);

            if (enSuelo && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)))
            {

                Debug.Log("SALTO");
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fuerzaSalto));
                animator.SetBool("enSuelo", enSuelo);
                // UpdateState("saltar");
                Debug.Log("Cambia animacion");
                animator.Play("saltar");
                audioPlayer.clip = jump;
                audioPlayer.Play();
            }

            //Agacharse solo BigMario
            if (enSuelo && Input.GetKeyDown(KeyCode.DownArrow))
            {

                Debug.Log("Agacharse");
               
                //animator.SetBool("enSuelo", enSuelo);
                // UpdateState("agachar");
               // animator.Play("agachar");
            }
        }
	}

    public void UpdateState(string state) {

            animator.Play(state);
        
    }

    public void Kill() {
       animator.Play("caer");
       GameCanvas.GetComponent<AudioSource>().Stop();
       GameManager.sharedInstance.GameOver();      
    }

    public void Win()
    {
        GameCanvas.GetComponent<AudioSource>().Stop();
        GameManager.sharedInstance.Winner();
       
    }

    //Testear plataformas cuando desapareces en pantalla vuelves a aparecer al principio
    void OnBecameInvisible()
    {
        //transform.position = new Vector3(-10,0,0);
        transform.position = new Vector3(-35, -28, 0);
    }

    //Particulas polvo suelo
    void PolvoPlay()
    {
        polvo.Play();
    }

    void PolvoStop()
    {
        polvo.Stop();
    }

    //Diparo
    public void Fire() {

        if (Input.GetButtonDown("Fuego")) {
            Debug.Log("creando bala");

            Instantiate(bulletPrefab, bulletSpawner.position, bulletSpawner.rotation);
        }    
    }   
     

    void OnTriggerEnter2D (Collider2D other)

    {
        Debug.Log("CHOCA CON ALGO");
        if (other.gameObject.tag == "enemigo")
        {
            Debug.Log("UN ENEMIGO");

            Kill();
            StartGame();
        }

    
        if (other.tag == "BloqueDestr") {
            Debug.Log("BLOQUE DESTRUIBLE");
        }
            
    }

    
}
