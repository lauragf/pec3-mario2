﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    private Rigidbody2D bulletRB;

    public float bulletSpeed;
    public float bulletLife;

    public GameObject player;
    public Transform playerTrans;

    public void Awake()
    {
        bulletRB = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTrans = player.transform;
    }
    
    void Start () {
        //direcion disparo
        if (playerTrans.localScale.x > 0)
        {
            bulletRB.velocity = new Vector2(bulletSpeed, bulletRB.velocity.y);
        }
        else {
            bulletRB.velocity = new Vector2(-bulletSpeed, bulletRB.velocity.y);
        }
    }
        
	
	void Update () {

        Destroy(gameObject, bulletLife );
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {



        if (other.gameObject.tag == "Muro" || other.gameObject.tag == "Tubo" )
        {
  ;
            Debug.Log("Destruir bala");
            Destroy(gameObject);

                               }


    }

}
