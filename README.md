PEC3 Mario2

Super Mario Bros Nes Mejorado

He decidido ir un poco mas alla con las colisiones y ver como se gestionan, para la interaccion con los enemigos y el entorno. Tambien he estado cambiando las animaciones y 
añadiendo musica de fondo. He dejado varias mecanicas sin acabar de crear como la de la seta de mario o el power up por qe no acabab de hacer bien las transiciones entre las animaciones
y las funcionalidades. 

He dejado el disparo siempre activado mediante la tecla "C" 

Lógica Aplicada

Juego simple de Plataformas "inspirado" en el Super Mario Bros de NES 

BUGs Detectados

- Al reiniciar el juego no cambia adecuadamente la animacion de muerte a stay

Mejoras Pendientes

- Solventar BUGS
- Crear nivel Oculto
- Implementar mecanica completa de bloques
- Crear bloques ocultos
- Implantar seta